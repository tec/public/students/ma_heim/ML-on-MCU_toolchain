# Machine Learning on Microcontrollers - Toolchain

This repository contains multiple Jupyter notebooks which were used for the implementation part of the master thesis [Evaluation and Deployment of Resource-Constrained Machine Learning on Embedded Devices](https://gitlab.ethz.ch/tec/public/students/ma_heim).

The notebooks are meant to be used in a consecutive order as outlined in the implementation chapter of the thesis. 
However, each notebook can also be used on its own or just carry out the required step.
The Markdown cells within the notebooks are used for describing the different plots.

![Toolchain](pictures/Visualization-toolchain.png)


## Installation

```
pip install -r requirements.txt
jupyter nbextension enable --py wigetsnbextension
```

### ARM Mbed OS

The `mbed-cli` should be installed as part of the `requirements.txt`. However, it requires mercurial (You need to adapt this for your OS.):
```
sudo apt-get install mercurial
```

If you encounter any problems follow the [guide by ARM](https://os.mbed.com/docs/mbed-os/v6.3/build-tools/install-and-set-up.html).

You have to install the GNU Toolchain for ARM manually, as ARM decided to not publish it to packet managers -- see [here](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm).



## Usage

```
jupyter lab
```

You will be automatically redirect to your browser where you can explore the notebooks with Jupyter lab.



## Relevant Repositories

This repository is part of the Master Thesis **Evaluation and Deployment of Resource-Constrained Machine Learning on Embedded Devices**.

- [Master Repository](https://gitlab.ethz.ch/tec/public/students/ma_heim/ML-on-MCU)
- [Toolchain implemented in Jupyter Notebooks and ARM Mbed](https://gitlab.ethz.ch/tec/public/students/ma_heim/ML-on-MCU_toolchain)
- [Evaluation implemented in Jupyter Notebooks](https://gitlab.ethz.ch/tec/public/students/ma_heim/ml-on-mcu_evaluation)
- [Evaluated Neural Network Models](https://gitlab.ethz.ch/tec/public/students/ma_heim/ml-on-mcu_models)
- [TensorFlow Lite Micro Mbed Project for Benchmarking Models with cmsis-nn](https://gitlab.ethz.ch/tec/public/students/ma_heim/tflu_benchmark-model_mbed_cmsis-nn)
- [TensorFlow Lite Micro Mbed Project for Benchmarking Models](https://gitlab.ethz.ch/tec/public/students/ma_heim/tflu_benchmark-model_mbed)


## License

*tbd*